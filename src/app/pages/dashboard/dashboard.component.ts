import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { AppinfoComponent } from '../login/appinfo/appinfo.component';
import { Storage } from '@ionic/storage';
import { ProfileComponent } from '../profile/profile.component';
import { ContactComponent } from '../contact/contact.component';
import { BookingdataService } from 'src/app/services/bookingdata.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],

})
export class DashboardComponent implements OnInit {

  userRole: any;
  serviceFlow: boolean = true;
  colSlot: number = 0;
  dashboardList: any[] = [];
  // infoMenu: any;
  keyType: string = 'info';
  label: any;
  userData: any = {
    name: ''
  };

  config: any = {
    small_logo: 'assets/imgs/yoga.jpg'
  };

  svgIcos: any;

  public passedObj:any;

  public isGuest=false ;

  constructor(public commonprovider: CommonProvider,
    public appsettings: AppSettings,public router:Router,private storage:Storage,
    private bookingdata:BookingdataService,public route:ActivatedRoute
    ) 
    { 
      this.label = this.commonprovider.getLabels().dashboard;

      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.passedObj = this.router.getCurrentNavigation().extras.state;

          this.customLoad();

        }
      });
      console.log("some step");

    }

  ngOnInit() {
    this.initData();
    this.config = this.commonprovider.getConfig();

    this.eventSubscribe();
  }

  customLoad()
  {
    this.initData();
    this.config = this.commonprovider.getConfig();
  }

  ionViewWillLoad() {
    
   }

  ionViewDidLoad() 
  {
    this.initData();
    this.config = this.commonprovider.getConfig();
  }

  ionViewDidEnter() {
    this.colSlot = 0;
    
  }

  initData() 
  {
    this.dashboardList = [];
    if(this.passedObj != undefined)
    {
      this.userData = this.passedObj.userdata ;
    }
    else
    {
      this.userData = this.commonprovider.getUserData();
    }

    //check if guest or not 

    if(this.userData != null && this.userData.id == 0) //usually chec the iser id .;ater
    {
        this.isGuest = true;
    }
    
    this.userRole = this.userData.user_role;
    this.svgIcos = this.commonprovider.getSvgIcos();
    // this.infoMenu = this.getInfoMenu();
    this.dashboardList = this.userData.user_role
      ? this.getTherapistMenus()
      : this.getClientMenus();

    let count = this.userData.user_role ? 2 : 3;

    this.dashboardList.push(this.getInfoMenu());
    this.dashboardList = this.chunkArray(this.dashboardList, count);

    let url = this.appsettings.userProfile
      + '?key=' + this.userData.auth
      + '&id=' + this.userData.id;

    if(!this.isGuest)
    {
      /***
       * if guest login is s present then set the storage to the hardcoded one 
       * passed from the login else used this service to get the user data 
       */
      this.commonprovider
      .get(url)
      .subscribe((resp: any) => {
        if (!resp.err_code) {
          this.setUserData(resp);
        } else {
          this.commonprovider.showToast(resp.err_msg);
        }
        this.commonprovider.hideLoader();
      });
  
    }
    else
    {
        this.setUserData(this.userData);
    }
  }  
    
  

  setUserData(resp) {
    this.userData = Object.assign(this.userData, resp.data);
    this.commonprovider.setUserData(this.userData);

    if (!this.userData.user_role) {
      this.setProfileMessage();
    }
  }

  chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
      let myChunk = myArray.slice(index, index + chunk_size);
      // Do something if you want with the group
      tempArray.push(myChunk);
    }

    return tempArray;
  }

  openPage(item: any) {
    let TIME_IN_MS = 190;
    this.colSlot = item.id;

    if(item.isDisabled)
    {
      this.commonprovider.showToast('Access denied. You need to log in first.');
      return;
    }

    setTimeout(() => {
      if (item.params) {
        let obj = {};
        obj[item.paramsKey] = this[item.paramsValue];
        //this.navCtrl.push(item.pageModule, obj);

        let navigationExtras: NavigationExtras = {
          state: {
            userdata: obj
          }
        };

        /****
         * code specific to booking module to pass data with service 
         */
        
        this.bookingdata.setData(navigationExtras); 
        this.router.navigate(["pages",item.pageName],navigationExtras);

      } else {
        //this.navCtrl.push(item.pageModule);
        this.router.navigate(["pages",item.pageName]);
      }
    }, TIME_IN_MS);
  }

  logOut() {
    
    if(this.userData.auth == "")
    {
      this.commonprovider.removeStorage();
      this.router.navigate(["login"]);
      return ;
    }

    this.commonprovider.Alert
      .confirm(this.label.confirmLogout)
      .then(
        (res: any) => {
          let dataObj = {
            'key': this.userData.auth,
            'id': this.userData.id,
            'packageName': this.config.package_name
          }
          this.commonprovider.post(this.appsettings.appLogout, dataObj).subscribe((resp: any) => {
            if (!resp.err_code) {
              this.commonprovider.removeStorage();
              //this.navCtrl.setRoot(LoginPage);
              this.router.navigate(["login"]);
            } else {
              this.commonprovider.showToast(resp.err_msg);
            }
            this.commonprovider.hideLoader();
          });
        },
        err => {
          this.commonprovider.showToast(err);
        }
      );
  }

  getClientMenus() {
    return [
      {
        id: 1,
        name: this.label.userServices,
        icon: this.svgIcos.list.data,
        color: '#526cd0',
        //pageModule: ServicesModal,,
        //pageName:'services',
        pageName:'bookAppointment',
        params: true,
        paramsKey: 'serviceFlow',
        paramsValue: 'serviceFlow',
        isDisabled:this.isGuest?false:false ,
      }, {
        id: 2,
        name: this.label.userBooking,
        icon: this.svgIcos.calendar.data,
        color: '#526cd0',
        //pageModule: BookingPage,
        pageName:'bookAppointment',
        params: true,
        paramsKey: 'serviceFlow',
        paramsValue: 'serviceFlow',
        isDisabled:this.isGuest?false:false,
      },
      {
        id: 3,
        name: this.label.userMyBooking,
        icon: this.svgIcos.home.data,
        color: '#EF820D',
        //pageModule: BookingTabPage,
        pageName:'booking',
        params: true,
        paramsKey: 'userRole',
        paramsValue: 'userRole',
        isDisabled:this.isGuest?true:false,
      },
      {
        id: 4,
        name: this.label.userMyProfile,
        icon: this.svgIcos.setting.data,
        color: '#ff527b',
        pageModule: ProfileComponent,
        pageName:'profile',
        params: true,
        paramsKey: 'therapistProfile',
        paramsValue: 'userData',
        infoMsg: false,
        isDisabled:this.isGuest?true:false,
      },
      {
        id: 5,
        name: this.label.userContact,
        icon: this.svgIcos.user.data,
        color: '#ff527b',
        pageModule: ContactComponent,
        pageName:'contact',
        params: true,
        paramsKey: 'contactData',
        paramsValue: 'userData',
        isDisabled:this.isGuest?true:false,
      }
    ];
  }

  getTherapistMenus() {
    return [
      {
        id: 6,
        name: this.label.empolyeeViewAppointments,
        iconType: this.svgIcos.analytics.type,
        icon: this.svgIcos.analytics.data,
        color: '#526cd0',
        //pageModule: BookingTabPage,
        pageName:'booking',
        params: true,
        paramsKey: 'userRole',
        paramsValue: 'userRole'
      },
      {
        id: 7,
        name: this.label.empolyeeMyAppointments,
        icon: this.svgIcos.calendar.data,
        color: '#EF820D',
        //pageModule: BlockOutPage
        pageName:"blockout"
      },
      {
        id: 8,
        name: this.label.empolyeeProfile,
        icon: this.svgIcos.setting.data,
        color: '#ff527b',
        pageModule: ProfileComponent,
        pageName:'profile',
        params: true,
        paramsKey: 'therapistProfile',
        paramsValue: 'userData'
      }
    ];
  }

  getInfoMenu() {
    return {
      id: 9,
      name: this.label.info,
      icon: this.svgIcos.info.data,
      color: '#0080FF',
      pageModule: AppinfoComponent,
      pageName:'info',
      params: true,
      paramsKey: 'type',
      paramsValue: 'keyType'
    };
  }

  

  setProfileMessage() {
    let user_info = this.userData.user_info;
    let isEmpty = false;

    Object.keys(user_info).forEach(key => {
      if (user_info[key] === null || user_info[key] === undefined)
        isEmpty = true;
    });

    this.dashboardList[1][0].infoMsg = isEmpty ? this.label.updateProfile : isEmpty;
  }

  
  eventSubscribe() 
  {
      this.commonprovider.guestToLoggedin.subscribe((obj)=>
      {
        if(obj && obj.loggedIndUid != 0 )
        {
          this.passedObj = undefined ;
          this.isGuest = false;
          setTimeout(() => {           // set timeout is for state propogation
            this.initData();
           }, 2);  
        }
        //else
        //this.showTherepistBtn = false;
             
      });

  }

  
}
