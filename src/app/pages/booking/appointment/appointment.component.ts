import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { AppSettings } from 'src/app/services/app-settings';
import { CommonProvider } from 'src/app/services/common/common';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss'],
})
export class AppointmentComponent implements OnInit {

  appointmentList: any = [];

  limit: number = 9;
  totalCount: number = 0;
  limitstart: number = 0;
  datalength: any = false;
  isLoadMore: boolean = true;
  filterData: any;
  label: any;
  config: any;
  isFilterSet: boolean = false;
  filterType:any;

  constructor(    public view: ModalController,
    public datepipe: DatePipe,
    public alertCtrl: AlertController,
    public appsettings: AppSettings,
    public commonprovider: CommonProvider,public params: NavParams
    ) 
    { 
      this.filterType = params.data.id ;

      this.filterData = params.data.date ;
      this.label = this.commonprovider.getLabels().appointment;
      this.config = this.commonprovider.getConfig();
  
    }

  ngOnInit() 
  {
    this.filterData = this.params.get('data'); //commented by prashant
    this.isFilterSet = this.filterData ? true : false;

    this.getAppointments(this.limit, this.limitstart);
  }

  getAppointments(limit: number, limitstart: number) {
    let dateFilter = this.filterData ?
      '&date=' + this.filterData.date : '';
    let url = this.appsettings.getAppointment
      + '?did=' + this.commonprovider.getUserLoginId()
      + '&limit=' + limit
      + '&limitstart=' + limitstart
      + '&filterType=' + this.filterType
      + dateFilter;

    this.commonprovider.showLoader(this.appsettings.appointmentLoader);
    this.commonprovider
      .get(url)
      .subscribe(
        (response: any) => {
          if (!response.err_code && response.data.count != 0) {
            this.appointmentList = this.appointmentList.concat(response.data.orders);

            this.totalCount = this.limitstart + this.limit;
            if (this.appointmentList.length == response.data.count) {
              this.isLoadMore = false;
            }

            this.commonprovider.hideLoader();
          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(
              response.err_msg || 'No record found !',
              5000
            );
          }
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );
  }

  formatInTime(dateString) {
    return dateString.substring(11);
  }

  doInfinite(ev: any) {
    setTimeout(() => {
      this.limitstart = this.totalCount;
      this.getAppointments(this.limit, this.limitstart);
      ev.complete();
    }, 1000);
  }

  showDetailsToaster(item: any) {
    let title = this.label.title;
    let time = this.datepipe.transform(item.start_time, 'HH:mm');
    let msg = '<div>'
      + '<b>Name</b> : ' + item.order_name + '<br>'
      + '<b>Phone no.</b> : ' + item.order_phone + '<br>'
      + '<b>Time</b> : ' + time + '<br>'
      + '<b>Note</b> : ' + item.order_notes + '<br>'
      + '<b>Fees </b>   : ' + this.config.currency_symbol + item.order_final_cost + '<br>'
      + '<b>Service name </b>: ' + item.service_detail.service_name + '<br>'
      + '</div>';

    this.commonprovider.showAlert(title, msg);
  }

  closeModal() {
    this.view.dismiss();
  }

  
  goToBackUrl()
  {
    
  }


}
