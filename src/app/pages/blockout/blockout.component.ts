import { Component, OnInit } from '@angular/core';
import { ModalController, Events } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { AppSettings } from 'src/app/services/app-settings';
import { CommonProvider } from 'src/app/services/common/common';
import { AppointmentComponent } from '../booking/appointment/appointment.component';
import { Location } from '@angular/common';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';

@Component({
  selector: 'app-blockout',
  templateUrl: './blockout.component.html',
  styleUrls: ['./blockout.component.scss'],
})
export class BlockoutComponent implements OnInit {

  therapist: any = {
    userId: '121',
    name: 'Harry Potter',
    profileImg: 'assets/imgs/user.png',
    speality: 'PAEDIATRICS, CONSULTANT',
    selected: false
  };

  blockOutList: any[];
  limit: number = 10;
  limitstart: string = '';
  isLoadMore: boolean = true;
  todayDate: any;
  dateFilter: any;
  label: any;
  config: any = {
    type: 'icon',
    title: 'Select Date Range',
    icon: 'md-options', //'md-calendar',
    pickMode: 'range'
  };
  doctorIcon: any;

  constructor(public modal: ModalController,
    public datepipe: DatePipe,
    public appsettings: AppSettings,
    public commonprovider: CommonProvider,
    public location:Location,
    public events: Events) 
    {
      this.label = this.commonprovider.getLabels().blockOut;
      this.blockOutList = [];
      this.doctorIcon = this.commonprovider.getSvgIcos().doctor.data;
  
      this.todayDate = new Date();
      this.todayDate = this.datepipe.transform(this.todayDate, 'yyyy-MM-dd');
  
      this.getBlockout(this.limit, this.limitstart);
      this.eventSubscribe();
      
     }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.events.unsubscribe('date:selected');
  }

  eventSubscribe() {
    this.events.subscribe('date:selected', (dateFilter) => {
      if (dateFilter.from) {
        this.isLoadMore = true;
        this.limitstart = dateFilter.from.string;
        this.dateFilter = dateFilter;
        this.blockOutList = [];
        this.getBlockout(this.limit, this.limitstart, this.dateFilter);
      }
    });
  }

  getBlockout(limit: number, limitstart: string, dateFilter?: any) {
    let filterUrl = '';
    if (dateFilter && dateFilter.from) {
      filterUrl = '&start_date=' + dateFilter.from.string
        + '&end_date=' + dateFilter.to.string;
    }

    let url = this.appsettings.getBlockout
      + '&did=' + this.commonprovider.getUserLoginId()
      + '&limit=' + limit
      + '&limitstart=' + limitstart + filterUrl;

    this.commonprovider.showLoader(this.appsettings.appointmentLoader);
    this.commonprovider
      .get(url)
      .subscribe(
        (response: any) => {
          if (!response.err_code && response.data.count != 0) {
            this.blockOutList = this.blockOutList.concat(response.data.block_out_data);

            if (response.data.block_out_data.length < this.limit) {
              this.isLoadMore = false;
            }

            this.commonprovider.hideLoader();
          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(
              response.err_msg || 'No record found !',
              5000
            );
          }
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );
  }

  doInfinite(ev: any) {
    setTimeout(() => {
      let index = this.blockOutList.length - 1;
      let dateStart = new Date(this.blockOutList[index].date);
      dateStart.setDate(dateStart.getDate() + 1);
      this.limitstart = this.datepipe.transform(dateStart, 'yyyy-MM-dd');
      this.getBlockout(this.limit, this.limitstart, this.dateFilter);
      ev.complete();
    }, 1000);
  }

  async showViewAppointment(item?: any) {
    if (item.attendees !== 0) {
      let obj: any = {
        date: item.date
      };

      const appointmentModal = await this.modal
        .create({component:AppointmentComponent,componentProps:{ data: obj }});

        appointmentModal.present();
      appointmentModal.onDidDismiss().then(data => {
        console.log('onDidDismiss', data);
      });
    }
    else {
      this.commonprovider.showToast(this.label.emptyAttendees, 1000);
    }
  }

  goToBackUrl()
  {
    this.location.back();
  }

  async openCalendar() {
    const options: CalendarModalOptions = {
      pickMode: 'range',
      title: 'RANGE'
    };

    let myCalendar = await this.modal.create({
        component: CalendarModal,
        componentProps: { options: options }
      });

      myCalendar.onDidDismiss().then(data => {
      
        if (data && data.data != undefined) 
        {  
          if (data.data.from) {
            this.isLoadMore = true;
            this.limitstart = data.data.from.string;
            this.dateFilter = data.data;
            this.blockOutList = [];
            this.getBlockout(this.limit, this.limitstart, this.dateFilter);
          }
          
        }
  
      });
    

    return await myCalendar.present();

  }
}
